<?php
declare(strict_types=1);

namespace App\Controller;

use App\Entity\VinylMix;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class MixController extends AbstractController
{
    public function __construct(private readonly EntityManagerInterface $entityManager)
    {
    }

    #[Route('/mix/new')]
    public function new(): Response
    {
        $genres = ['pop', 'rock'];
        $mix = new VinylMix();
        $mix
            ->setDescription('A pure mix of drummers turned singers!')
            ->setGenre($genres[array_rand($genres)])
            ->setTitle('Do you remember... Phil Collins?')
            ->setTrackCount(random_int(5, 20))
            ->setVotes(random_int(-50, 50));
        $this->entityManager->persist($mix);
        $this->entityManager->flush();

        return new Response(
            sprintf(
                'Mix %d is %d tracks of pure 80\'s heaven...',
                $mix->getId(),
                $mix->getTrackCount()
            )
        );
    }

    #[Route('/mix/{slug}', name: 'app_mix_show')]
    public function show(VinylMix $mix): Response
    {
        return $this->renderForm('mix/show.html.twig', [
            'mix' => $mix,
        ]);
    }

    #[Route('/mix/{id}/vote', name: 'app_mix_vote', methods: ['POST'])]
    public function vote(EntityManagerInterface $entityManager, Request $request, VinylMix $mix): Response
    {
        $direction = $request->request->get('direction', 'up');
        if ($direction === 'up') {
            $mix->upVote();
        } else {
            $mix->downVote();
        }
        $entityManager->flush();

        $this->addFlash('success', 'Vote counted!');

        return $this->redirectToRoute('app_mix_show', [
            'slug' => $mix->getSlug(),
        ]);
    }
}
